# Require any additional compass plugins here.
require 'susy'
require 'compass-growl'

# Set this to the root of your project when deployed:
http_path = '/'
css_dir = 'components/css'
sass_dir = 'components/sass'
images_dir = 'components/images'
javascripts_dir = 'components/js'

# Path to our library Sass files
add_import_path 'library/components/sass'

preferred_syntax = :scss

disable_warnings = true

# Enable Sass Sleuth (http://goo.gl/LktW0)
# Note: If you are using Firefox, give FireSass a try: http://goo.gl/XLdfb
# if environment != :production
#   sass_options = {
#     :debug_info => true
#   }
# end
